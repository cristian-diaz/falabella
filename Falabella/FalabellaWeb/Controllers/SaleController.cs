﻿using Deal.Tables;
using Entities.Tables;
using FalabellaWeb.CookieHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FalabellaWeb.Controllers
{
    [Authorize]
    public class SaleController : Controller
    {
        private readonly ICookieContainerCD cookieContainerCD;
        private ClientDeal _clientDeal;
        private ProductDeal _productDeal;
        private SaleDeal _saleDeal;
        public SaleController()
        {
            cookieContainerCD = new CookieContainerCD();
            _clientDeal = new ClientDeal((string)cookieContainerCD.DB);
            _productDeal = new ProductDeal((string)cookieContainerCD.DB);
            _saleDeal = new SaleDeal((string)cookieContainerCD.DB);
        }
        // GET: Sale
        public ActionResult Index()
        {
            ViewData["ddl_Client"] = new SelectList(_clientDeal.ConsultClients(), "Code", "Name");
            ViewData["ddl_Product"] = new SelectList(_productDeal.ConsultProducts(), "Code", "Name");
            return View();
        }

        public ActionResult CreateSale(Sale Sale)
        {
            try
            {
                Sale.DateSale = DateTime.Now;
                Sale.Status = "A";
                var answer = _saleDeal.Create(Sale);
                return Json(new { success = true, responseText = "Ok" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}