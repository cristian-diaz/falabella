﻿using Deal.Tables;
using Entities.Tables;
using FalabellaWeb.CookieHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FalabellaWeb.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private readonly ICookieContainerCD cookieContainerCD;
        private ProductDeal _productDeal;
        public ProductController()
        {
            cookieContainerCD = new CookieContainerCD();
            _productDeal = new ProductDeal((string)cookieContainerCD.DB);
        }
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GridProducts()
        {
            List<Product> data = new List<Product>();
            try
            {
                data = _productDeal.ConsultProducts();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return PartialView(data);
        }

        public ActionResult GridProductProtection(string Code)
        {
            List<Protection> data = new List<Protection>();
            try
            {
                data = _productDeal.ConsultProductProtection(Code);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return PartialView(data);
        }
    }
}