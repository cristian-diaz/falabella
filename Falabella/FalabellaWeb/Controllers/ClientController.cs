﻿using Deal.Tables;
using Entities.Tables;
using FalabellaWeb.CookieHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FalabellaWeb.Controllers
{
    [Authorize]
    public class ClientController : Controller
    {
        private readonly ICookieContainerCD cookieContainerCD;
        private ClientDeal _clientDeal;
        public ClientController()
        {
            cookieContainerCD = new CookieContainerCD();
            _clientDeal = new ClientDeal((string)cookieContainerCD.DB);
        }
        // GET: Client
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateClient(Client Client)
        {
            try
            {
                var answer = _clientDeal.Create(Client);
                return Json(new { success = true, responseText = "Ok" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}