﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FalabellaWeb.CookieHttp
{
    public class CookieContainerCD : ICookieContainerCD
    {
        private const string ApiTokenKey = "ApiToken";
        private const string DBString = "DB";

        public object ApiCookie
        {
            get { return Current.Session != null ? Current.Session[ApiTokenKey] : null; }
            set { if (Current.Session != null) Current.Session.Timeout = 30; Current.Session[ApiTokenKey] = value; }
        }

        public object DB
        {
            get { return Current.Session != null ? Current.Session[DBString] : null; }
            set { if (Current.Session != null) Current.Session.Timeout = 30; Current.Session[DBString] = value; }
        }

        private static HttpContextBase Current
        {
            get { return new HttpContextWrapper(HttpContext.Current); }
        }
    }
}