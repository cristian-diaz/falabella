﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FalabellaWeb.CookieHttp
{
    public interface ICookieContainerCD
    {
        object ApiCookie { get; set; }

        object DB { get; set; }
    }
}
