﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FalabellaWeb.Startup))]
namespace FalabellaWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
