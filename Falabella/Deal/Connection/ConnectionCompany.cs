﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Deal.Connection
{
    public class ConnectionCompany
    {
        private readonly SqlConnection _conexion;

        public SqlConnection Conexion
        {
            get
            {
                return _conexion;
            }
        }

        public ConnectionCompany(string DB)
        {
            _conexion = new SqlConnection();
            Conexion.ConnectionString = ConfigurationManager.ConnectionStrings["CompanyConnection"].ConnectionString.Replace("{DBCOMPANY}", DB);
        }

        public void conectar()
        {
            try
            {
                if (Conexion.State != ConnectionState.Open)
                {
                    Conexion.Open();
                }
                if (Conexion == null)
                {
                    throw new Exception("Error. conexion nula!");
                }
                if (Conexion.State != ConnectionState.Open)
                {
                    throw new Exception("Error. conexion cerrada!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Imposible abrir conexion" + " " + ex.Message);
            }
        }

        public void CerrarConexion()
        {
            try
            {
                if ((Conexion != null))
                {
                    if (Conexion.State == ConnectionState.Open)
                    {
                        Conexion.Close();
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Imposible cerrar conexion" + ex.Message);
            }
        }

        //public DataSet Consultar(string sentencia)
        //{
        //    try
        //    {
        //        DataSet DS = new DataSet();
        //        conectar();
        //        OdbcCommand cmd = new OdbcCommand();
        //        cmd.CommandText = sentencia;
        //        OdbcDataAdapter Adaptador = new OdbcDataAdapter(cmd);
        //        Adaptador.Fill(DS);
        //        Adaptador.Dispose();
        //        return (DS);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //}

        public DataTable Consult(string queryString)
        {
            DataSet dataSet = new DataSet();
            using (SqlConnection connection = new SqlConnection(Conexion.ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand =
                    new SqlCommand(queryString, connection);
                SqlCommandBuilder builder =
                    new SqlCommandBuilder(adapter);

                connection.Open();

                adapter.Fill(dataSet);

                adapter.Update(dataSet);
            }
            DataTable dataTable = dataSet.Tables[0];
            return dataTable;
        }

        public int Execute(string queryString)
        {
            int r = 0;
            using (SqlConnection connection = new SqlConnection(Conexion.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(queryString, connection);
                connection.Open();
                r = cmd.ExecuteNonQuery();
            }
            return r;
        }

        public static List<T> ConvertirDataSetAList<T>(DataSet ds) where T : new()
        {
            DataTable dt = ds.Tables[0];
            List<T> listaObjects = new List<T>();
            Type t = typeof(T);
            foreach (DataRow dr in dt.Rows)
            {
                T item = new T();
                listaObjects.Add(item);
                foreach (DataColumn dc in dt.Columns)
                {
                    PropertyInfo pi = t.GetProperty(dc.ColumnName);
                    if (pi != null)
                    {
                        pi.SetValue(item, dr[dc.ColumnName], null);
                    }
                }
            }
            return listaObjects;
        }
    }
}
