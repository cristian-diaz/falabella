﻿using Deal.Connection;
using Entities.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deal.Tables
{
    public class ClientDeal
    {
        ConnectionCompany conDBC;
        public ClientDeal(string DB)
        {
            conDBC = new ConnectionCompany(DB);
        }

        public List<Client> ConsultClients()
        {
            List<Client> data = new List<Client>();
            try
            {
                var DT = conDBC.Consult("select * from Client");
                if (DT != null)
                {
                    var listdata = DT.AsEnumerable();
                    data = (from item in listdata
                             select new Client
                             {
                                 Code = item.Field<string>("Code"),
                                 Name = item.Field<string>("Name"),
                                 Gender = item.Field<string>("Gender"),
                                 Phone = item.Field<string>("Phone"),
                                 Type = item.Field<string>("Type"),
                                 TypeDocument = item.Field<string>("TypeDocument"),
                                 Address = item.Field<string>("Address")
                             }).ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return data;
        }

        public Client ConsultClient(string Code)
        {
            Client data = new Client();
            try
            {
                var DT = conDBC.Consult("select * from Client WHERE Code = '" + Code + "'");
                if (DT != null)
                {
                    var listdata = DT.AsEnumerable();
                    data = (from item in listdata
                             select new Client
                             {
                                 Code = item.Field<string>("Code"),
                                 Name = item.Field<string>("Name"),
                                 Gender = item.Field<string>("Gender"),
                                 Phone = item.Field<string>("Phone"),
                                 Type = item.Field<string>("Type"),
                                 TypeDocument = item.Field<string>("TypeDocument"),
                                 Address = item.Field<string>("Address")
                             }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return data;
        }

        public int Create(Client Client)
        {
            var r = 0;
            try
            {
                r = conDBC.Execute("exec CreateClient @Code = '" + Client.Code + "', @Type = '" + Client.Type + "', @TypeDocument = '" + Client.TypeDocument + "', @Name = '" + Client.Name + "', @Address = '" + Client.Address + "', @Phone = '" + Client.Phone + "', @Gender = '" + Client.Gender + "'");
            }
            catch (Exception ex)
            {

                throw;
            }

            return r;
        }
    }
}
