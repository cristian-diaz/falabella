﻿using Deal.Connection;
using Entities.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deal.Tables
{
    public class ProductDeal
    {
        ConnectionCompany conDBC;
        public ProductDeal(string DB)
        {
            conDBC = new ConnectionCompany(DB);
        }

        public List<Product> ConsultProducts()
        {
            List<Product> data = new List<Product>();
            try
            {
                var DT = conDBC.Consult("select * from Product");
                if (DT != null)
                {
                    var listdata = DT.AsEnumerable();
                    data = (from item in listdata
                            select new Product
                            {
                                Code = item.Field<string>("Code"),
                                Name = item.Field<string>("Name"),
                                Description = item.Field<string>("Description"),
                                Status = item.Field<string>("Status")
                            }).ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return data;
        }

        public Product ConsultProduct(string Code)
        {
            Product data = new Product();
            try
            {
                var DT = conDBC.Consult("select * from Product WHERE Code = '" + Code + "'");
                if (DT != null)
                {
                    var listdata = DT.AsEnumerable();
                    data = (from item in listdata
                            select new Product
                            {
                                Code = item.Field<string>("Code"),
                                Name = item.Field<string>("Name"),
                                Description = item.Field<string>("Description"),
                                Status = item.Field<string>("Status")
                            }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return data;
        }

        public List<Protection> ConsultProductProtection(string Code)
        {
            List<Protection> data = new List<Protection>();
            try
            {
                var DT = conDBC.Consult("select PR.* from ProductProtection PP inner join Product P ON P.Code = PP.ProductCode inner join Protection PR ON PP.ProtectionCode = PR.Code Where P.Code = '" + Code + "'");
                if (DT != null)
                {
                    var listdata = DT.AsEnumerable();
                    data = (from item in listdata
                            select new Protection
                            {
                                Code = item.Field<string>("Code"),
                                Name = item.Field<string>("Name"),
                                Description = item.Field<string>("Description"),
                                Type = item.Field<string>("Type"),
                                Status = item.Field<string>("Status")
                            }).ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return data;
        }
    }
}
