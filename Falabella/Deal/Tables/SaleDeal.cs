﻿using Deal.Connection;
using Entities.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deal.Tables
{
    public class SaleDeal
    {
        ConnectionCompany conDBC;
        public SaleDeal(string DB)
        {
            conDBC = new ConnectionCompany(DB);
        }

        public int Create(Sale Sale)
        {
            var r = 0;
            try
            {
                r = conDBC.Execute("exec CreateSale @DateSale = '" + Sale.DateSale.ToString("MM/dd/yyyy") + "', @ClientCode = '" + Sale.ClientCode + "', @ProductCode = '" + Sale.@ProductCode + "', @LifeInitial = '" + Sale.LifeInitial.ToString("MM/dd/yyyy") + "', @LifeFinal = '" + Sale.LifeFinal.ToString("MM/dd/yyyy") + "', @TotalSale = " + Sale.TotalSale + ", @Status = '" + Sale.Status + "'");
            }
            catch (Exception ex)
            {

                throw;
            }

            return r;
        }
    }
}
