﻿using Deal.Connection;
using Entities.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deal.Tables
{
    public class CompanyDeal
    {
        ConnectionDefault conDB;
        public CompanyDeal()
        {
            conDB = new ConnectionDefault();
        }

        public List<Company> ConsultCompany()
        {
            List<Company> data = new List<Company>();
            try
            {
                var DT = conDB.Consult("select * from Company");
                if (DT != null)
                {
                    var listdata = DT.AsEnumerable();
                    data = (from item in listdata
                             select new Company
                             {
                                 Code = item.Field<string>("Code"),
                                 Name = item.Field<string>("Name"),
                                 Status = item.Field<string>("Status")
                             }).ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return data;
        }
    }
}
