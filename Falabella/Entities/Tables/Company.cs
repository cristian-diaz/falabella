﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Tables
{
    public class Company
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }
}
