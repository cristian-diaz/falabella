﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Tables
{
    public class Sale
    {
        public DateTime DateSale { get; set; }
        public string ClientCode { get; set; }
        public string ProductCode { get; set; }
        public DateTime LifeInitial { get; set; }
        public DateTime LifeFinal { get; set; }
        public decimal TotalSale { get; set; }
        public string Status { get; set; }
    }
}
