﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Common
{
    public class User
    {
        [Required]
        [Display(Name = "Empresa")]
        public string CompanyDB { get; set; }

        [Required]
        [Display(Name = "Nombre usuario")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Display(Name = "Recordarme")]
        public bool RememberMe { get; set; }
    }
}
