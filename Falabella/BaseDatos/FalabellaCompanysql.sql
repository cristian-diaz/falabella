create database Company1
go
use Company1
go
GO
create table TableCommon
(
Code nvarchar(20) primary key,
Name nvarchar(100)
)
GO
create table FieldsCommon
(
Id bigint identity(1,1) primary key,
TableCode nvarchar(20),
Value nvarchar(5),
Name nvarchar(100),
[Order] int
)
GO
alter table FieldsCommon add foreign key (TableCode) references TableCommon(Code)
GO
create table Product
(
Code nvarchar(50) primary key,
Name nvarchar(200),
[Description] nvarchar(1000),
[Status] nvarchar(5)
)
GO
insert into Product values ('SE001', 'Seguro 1', 'Seguro todo riesgo', 'A')
GO
create table Protection
(
Code nvarchar(50) primary key,
Name nvarchar(200),
[Description] nvarchar(1000),
[Type] nvarchar(10),
[Status] nvarchar(5)
)
GO
insert into Protection values ('AM001', 'Amparo 1', 'Caso accidente', 'S', 'A')
insert into Protection values ('AM002', 'Amparo 2', 'Asistencia Juridica', 'S', 'A')
GO
create table ProductProtection
(
Id bigint identity(1,1) primary key,
ProductCode nvarchar(50),
ProtectionCode nvarchar(50),
Value decimal,
Percentage decimal,
MinSMMLLV decimal
)
GO
insert into ProductProtection values ('SE001', 'AM001', 15000000, 10, 1)
insert into ProductProtection values ('SE001', 'AM002', 0, 0, 0)
GO
alter table ProductProtection add foreign key (ProductCode) references Product(Code)
GO
alter table ProductProtection add foreign key (ProtectionCode) references Protection(Code)
GO
create table Client
(
Code nvarchar(50) primary key,
Type nvarchar(10),
TypeDocument nvarchar(10),
Name nvarchar(150),
[Address] nvarchar(100),
Phone nvarchar(30),
Gender nvarchar(5),
)
GO
insert into Client values ('123', 'P', 'CC', 'Cristian Diaz', 'Cra 23', '12413', 'H')
GO
create table Sale
(
Id bigint identity(1,1) primary key,
DateSale datetime,
ClientCode nvarchar(50),
ProductCode nvarchar(50),
LifeInitial datetime,
LifeFinal datetime,
TotalSale decimal,
[Status] nvarchar(5)
)
GO
alter table Sale add foreign key (ClientCode) references Client(Code)
alter table Sale add foreign key (ProductCode) references Product(Code)
GO
CREATE PROCEDURE CreateSale
@DateSale datetime,
@ClientCode nvarchar(50),
@ProductCode nvarchar(50),
@LifeInitial datetime,
@LifeFinal datetime,
@TotalSale decimal,
@Status nvarchar(5)
as
begin
 INSERT INTO Sale values(@DateSale,@ClientCode,@ProductCode,@LifeInitial,@LifeFinal,@TotalSale,@Status)
End
GO
CREATE PROCEDURE CreateClient
@Code nvarchar(50),
@Type nvarchar(10),
@TypeDocument nvarchar(10),
@Name nvarchar(150),
@Address nvarchar(100),
@Phone nvarchar(30),
@Gender nvarchar(5)
AS
BEGIN
	INSERT INTO Client VALUES (@Code, @Type, @TypeDocument, @Name, @Address, @Phone, @Gender)
END