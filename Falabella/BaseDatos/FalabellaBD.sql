USE Falabella
GO
create table Company
(
Code nvarchar(50) primary key,
Name nvarchar(150) not null,
Status nvarchar(5) not null
)
GO
INSERT INTO Company values ('Company1', 'Compa�ia 1', 'A')
INSERT INTO Company values ('Company2', 'Compa�ia 2', 'A')
INSERT INTO Company values ('Company3', 'Compa�ia 3', 'A')