create database Company2
go
use Company2
go
GO
create table TableCommon
(
Code nvarchar(20) primary key,
Name nvarchar(100)
)
GO
create table FieldsCommon
(
Id bigint identity(1,1) primary key,
TableCode nvarchar(20),
Value nvarchar(5),
Name nvarchar(100),
[Order] int
)
GO
alter table FieldsCommon add foreign key (TableCode) references TableCommon(Code)
GO
create table Product
(
Code nvarchar(50) primary key,
Name nvarchar(200),
[Description] nvarchar(1000),
[Status] nvarchar(5)
)
GO
create table Protection
(
Code nvarchar(50) primary key,
Name nvarchar(200),
[Description] nvarchar(1000),
[Type] nvarchar(10),
[Status] nvarchar(5)
)
GO
create table ProductProtection
(
Id bigint identity(1,1) primary key,
ProductCode nvarchar(50),
ProtectionCode nvarchar(50),
Value decimal,
Percentage decimal,
MinSMMLLV decimal
)
GO
alter table ProductProtection add foreign key (ProductCode) references Product(Code)
GO
alter table ProductProtection add foreign key (ProtectionCode) references Protection(Code)
GO
create table Client
(
Code nvarchar(50) primary key,
Type nvarchar(10),
TypeDocument nvarchar(10),
Name nvarchar(150),
[Address] nvarchar(100),
Phone nvarchar(30),
Gender nvarchar(5),
)
GO
create table Sale
(
Id bigint identity(1,1) primary key,
DateSale datetime,
ClientCode nvarchar(50),
ProductCode nvarchar(50),
LifeInitial datetime,
LifeFinal datetime,
TotalSale decimal,
[Status] nvarchar(5)
)
GO
alter table Sale add foreign key (ClientCode) references Client(Code)
alter table Sale add foreign key (ProductCode) references Product(Code)
